from setuptools import setup
setup(
    name='projectconfig',
    entry_points={
        'console_scripts': ['manage=projectconfig.manage:main']
    },
)
